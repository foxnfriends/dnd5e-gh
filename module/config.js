import { ClassFeaturesGH } from "./classFeatures.js";

export const GH5E = {};

GH5E.classFeatures = ClassFeaturesGH;

GH5E.armorClasses = {
    unarmoredCollegeOfAdventurersMonk: {
        label: "GH5E.UnarmoredCollegeOfAdventurersMonk",
        formula: "10 + @abilities.dex.mod + @abilities.cha.mod"
    }
}

/**
 * Special character flags.
 * @enum {{
 *   name: string,
 *   hint: string,
 *   [abilities]: string[],
 *   [skills]: string[],
 *   section: string,
 *   type: any,
 *   placeholder: any
 * }}
 */
GH5E.characterFlags = {
    potentSpellcasting: {
        name: "GH5E.FlagsPotentSpellcasting",
        hint: "GH5E.FlagsPotentSpellcastingHint",
        section: "GH5E.Feats",
        type: Boolean
    },
}