import Item5e from "../../../../systems/dnd5e/module/item/entity.js";
import {simplifyRollFormula, d20Roll, damageRoll} from "../../../../systems/dnd5e/module/dice.js";

export default class Item5eGH extends Item5e {
    /**
   * Apply listeners to chat messages.
   * @param {HTML} html  Rendered chat message.
   */
     static chatListeners(html) {
      html.on("click", ".card-buttons button", this._onChatCardAction.bind(this));
      html.on("click", ".item-name", this._onChatCardToggleContent.bind(this));
    }
  
    /* -------------------------------------------- */
  
    /**
     * Handle execution of a chat card action via a click event on one of the card buttons
     * @param {Event} event       The originating click event
     * @returns {Promise}         A promise which resolves once the handler workflow is complete
     * @private
     */
    static async _onChatCardAction(event) {
      event.preventDefault();
  
      // Extract card data
      const button = event.currentTarget;
      button.disabled = true;
      const card = button.closest(".chat-card");
      const messageId = card.closest(".message").dataset.messageId;
      const message = game.messages.get(messageId);
      const action = button.dataset.action;
  
      // Validate permission to proceed with the roll
      const isTargetted = action === "save";
      if ( !( isTargetted || game.user.isGM || message.isAuthor ) ) return;
  
      // Recover the actor for the chat card
      const actor = await this._getChatCardActor(card);
      if ( !actor ) return;
  
      // Get the Item from stored flag data or by the item ID on the Actor
      const storedData = message.getFlag("dnd5e-gh", "itemData");
      const item = storedData ? new this(storedData, {parent: actor}) : actor.items.get(card.dataset.itemId);
      if ( !item ) {
        return ui.notifications.error(game.i18n.format("DND5E.ActionWarningNoItem", {item: card.dataset.itemId, name: actor.name}));
      }
      const spellLevel = parseInt(card.dataset.spellLevel) || null;
  
      // Handle different actions
      let targets;
      switch ( action ) {
        case "attack":
          await item.rollAttack({event}); break;
        case "damage":
        case "versatile":
          await item.rollDamage({
            critical: event.altKey,
            event: event,
            spellLevel: spellLevel,
            versatile: action === "versatile"
          });
          break;
        case "formula":
          await item.rollFormula({event, spellLevel}); break;
        case "save":
          targets = this._getChatCardTargets(card);
          for ( let token of targets ) {
            const speaker = ChatMessage.getSpeaker({scene: canvas.scene, token: token});
            await token.actor.rollAbilitySave(button.dataset.ability, { event, speaker });
          }
          break;
        case "toolCheck":
          await item.rollToolCheck({event}); break;
        case "placeTemplate":
          const template = game.dnd5e.canvas.AbilityTemplate.fromItem(item);
          if ( template ) template.drawPreview();
          break;
        case "abilityCheck":
          targets = this._getChatCardTargets(card);
          for ( let token of targets ) {
            const speaker = ChatMessage.getSpeaker({scene: canvas.scene, token: token});
            await token.actor.rollAbilityTest(button.dataset.ability, { event, speaker });
          }
          break;
      }
  
      // Re-enable the button
      button.disabled = false;
    }
  
    /* -------------------------------------------- */
  
    /**
     * Handle toggling the visibility of chat card content when the name is clicked
     * @param {Event} event   The originating click event
     * @private
     */
    static _onChatCardToggleContent(event) {
      event.preventDefault();
      const header = event.currentTarget;
      const card = header.closest(".chat-card");
      const content = card.querySelector(".card-content");
      content.style.display = content.style.display === "none" ? "block" : "none";
    }

    /**
   * Place a damage roll using an item (weapon, feat, spell, or equipment)
   * Rely upon the damageRoll logic for the core implementation.
   * @param {object} [config]
   * @param {MouseEvent} [config.event]    An event which triggered this roll, if any
   * @param {boolean} [config.critical]    Should damage be rolled as a critical hit?
   * @param {number} [config.spellLevel]   If the item is a spell, override the level for damage scaling
   * @param {boolean} [config.versatile]   If the item is a weapon, roll damage using the versatile formula
   * @param {object} [config.options]      Additional options passed to the damageRoll function
   * @returns {Promise<Roll>}        A Promise which resolves to the created Roll instance
   */
  rollDamage({critical=false, event=null, spellLevel=null, versatile=false, options={}}={}) {
    if ( !super.hasDamage ) throw new Error("You may not make a Damage Roll with this Item.");
    const itemData = this.data.data;
    const actorData = this.actor.data.data;
    const messageData = {"flags.dnd5e.roll": {type: "damage", itemId: this.id }};

    // Get roll data
    const parts = itemData.damage.parts.map(d => d[0]);
    const rollData = super.getRollData();
    if ( spellLevel ) rollData.item.level = spellLevel;

    // Configure the damage roll
    const actionFlavor = game.i18n.localize(itemData.actionType === "heal" ? "DND5E.Healing" : "DND5E.DamageRoll");
    const title = `${this.name} - ${actionFlavor}`;
    const rollConfig = {
      actor: this.actor,
      critical: critical ?? event?.altKey ?? false,
      data: rollData,
      event: event,
      fastForward: event ? event.shiftKey || event.altKey || event.ctrlKey || event.metaKey : false,
      parts: parts,
      title: title,
      flavor: this.labels.damageTypes.length ? `${title} (${this.labels.damageTypes})` : title,
      dialogOptions: {
        width: 400,
        top: event ? event.clientY - 80 : null,
        left: window.innerWidth - 710,
        speaker: ChatMessage.getSpeaker({actor: this.actor})
      },
      messageData: messageData
    };

    // Adjust damage from versatile usage
    if ( versatile && itemData.damage.versatile ) {
      parts[0] = itemData.damage.versatile;
      messageData["flags.dnd5e.roll"].versatile = true;
    }

    // Scale damage from up-casting spells
    if ( (this.data.type === "spell") ) {
      if ( (itemData.scaling.mode === "cantrip") ) {
        let level;
        if ( this.actor.type === "character" ) 
        {
            level = actorData.details.level;
            if(this.actor.getFlag("dnd5e", "potentSpellcasting"))
                parts.push("@abilities.wis.mod")
        }
        else if ( itemData.preparation.mode === "innate" ) level = Math.ceil(actorData.details.cr);
        else level = actorData.details.spellLevel;
        super._scaleCantripDamage(parts, itemData.scaling.formula, level, rollData);
      }
      else if ( spellLevel && (itemData.scaling.mode === "level") && itemData.scaling.formula ) {
        const scaling = itemData.scaling.formula;
        super._scaleSpellDamage(parts, itemData.level, spellLevel, scaling, rollData);
      }
    }

    // Add damage bonus formula
    const actorBonus = getProperty(actorData, `bonuses.${itemData.actionType}`) || {};
    if ( actorBonus.damage && (parseInt(actorBonus.damage) !== 0) ) {
      parts.push(actorBonus.damage);
    }

    // Handle ammunition damage
    const ammoData = this._ammo?.data;

    // Only add the ammunition damage if the ammution is a consumable with type 'ammo'
    if ( this._ammo && (ammoData.type === "consumable") && (ammoData.data.consumableType === "ammo") ) {
      parts.push("@ammo");
      rollData.ammo = ammoData.data.damage.parts.map(p => p[0]).join("+");
      rollConfig.flavor += ` [${this._ammo.name}]`;
      delete this._ammo;
    }

    // Factor in extra critical damage dice from the Barbarian's "Brutal Critical"
    if ( itemData.actionType === "mwak" ) {
      rollConfig.criticalBonusDice = this.actor.getFlag("dnd5e", "meleeCriticalDamageDice") ?? 0;
    }

    // Factor in extra weapon-specific critical damage
    if ( itemData.critical?.damage ) {
      rollConfig.criticalBonusDamage = itemData.critical.damage;
    }

    // Call the roll helper utility
    return damageRoll(mergeObject(rollConfig, options));
  }
}