export const ClassFeaturesGH = {
    barbarian: {
        subclasses: {
            "path-of-the-fractured": {
                label: "Path of the Fractured",
                source: "GH PG pg. 39",
                features: {
                    3: ["Compendium.dnd5e-gh.classfeatures-gh.VljAZ8x0IkW1UPlW","Compendium.dnd5e-gh.classfeatures-gh.NwEHg9I78XucQOeJ"],
                    6: ["Compendium.dnd5e-gh.classfeatures-gh.qQHwQUWalYMV6a6j"],
                    10: ["Compendium.dnd5e-gh.classfeatures-gh.fhqGJhG6HKtShVUf"],
                    14: ["Compendium.dnd5e-gh.classfeatures-gh.yQtffDqxC9UGOV1a"]
                }
            },
            "path-of-the-primal-spirit": {
                label: "Path of the Primal Spirit",
                source: "GH PG pg. 40",
                features: {
                    3: ["Compendium.dnd5e-gh.classfeatures-gh.GotQcrSZ1wy1lUbJ","Compendium.dnd5e-gh.classfeatures-gh.ZRdrT5Sz0Xtaly89"],
                    6: ["Compendium.dnd5e-gh.classfeatures-gh.a0hvyePUIuEPYO34"],
                    10: ["Compendium.dnd5e-gh.classfeatures-gh.rfjXMeBumcXjtNRS"],
                    14: ["Compendium.dnd5e-gh.classfeatures-gh.JPSHKguny6X1HtDM"]
                }
            }
        }
    },
    bard: {
        subclasses: {
            "college-of-adventurers": {
                label: "College of Adventurers",
                source: "GH PG pg. 42",
                features: {
                    3: ["Compendium.dnd5e-gh.classfeatures-gh.qqZY5dvSBoM9U5wR","Compendium.dnd5e-gh.classfeatures-gh.agp1kSwUcEOiO83u"],
                    6: ["Compendium.dnd5e-gh.classfeatures-gh.agp1kSwUcEOiO83u"],
                    14: ["Compendium.dnd5e-gh.classfeatures-gh.HHlKp5IgqdTgQtZ2"]
                }
            },
            "college-of-requiems": {
                label: "College of Requiems",
                source: "GH PG pg. 44",
                features: {
                    3: ["Compendium.dnd5e-gh.classfeatures-gh.HDv3OB75tkpxXZ7V","Compendium.dnd5e-gh.classfeatures-gh.dxWpCEwK8LKPUcbX"],
                    6: ["Compendium.dnd5e-gh.classfeatures-gh.GjZkyKiGVfnrCEIF"],
                    14: ["Compendium.dnd5e-gh.classfeatures-gh.aSRVCsWL7uwRazZD"]
                }
            }
        }
    },
    cleric: {
        subclasses: {
            "eldritch-domain": {
                label: "Eldritch Domain",
                source: "GH PG pg. 45",
                features: {
                    1: ["Compendium.dnd5e-gh.classfeatures-gh.dp6zj8mPO4wHhOvw","Compendium.dnd5e-gh.classfeatures-gh.mO2Nk3Su8BDHWAnw","Compendium.dnd5e-gh.classfeatures-gh.aW8u6yvp2M5OBIRZ"],
                    2: ["Compendium.dnd5e-gh.classfeatures-gh.wCLXLSyQEJIcVPuP"],
                    6: ["Compendium.dnd5e-gh.classfeatures-gh.5XpDULLxb8HDPG8X"],
                    8: ["Compendium.dnd5e-gh.classfeatures-gh.rR4uVl5vIRTv2uDT"],
                    17: ["Compendium.dnd5e-gh.classfeatures-gh.xSSjqJ46dOA56LOp"]
                }
            },
            "inquisition-domain": {
                label: "Inquisition Domain",
                source: "GH PG pg. 47",
                features: {
                    1: ["Compendium.dnd5e-gh.classfeatures-gh.cvU5GTQx1xjvJOyz","Compendium.dnd5e-gh.classfeatures-gh.F27jIGkxjfZyn5sD","Compendium.dnd5e-gh.classfeatures-gh.yEuLC4GlNSsPwztb"],
                    2: ["Compendium.dnd5e-gh.classfeatures-gh.zEkulQjxSbqYSwbO"],
                    6: ["Compendium.dnd5e-gh.classfeatures-gh.kfUvJepJo59MRpbt"],
                    8: ["Compendium.dnd5e-gh.classfeatures-gh.qIQ3cOyn35BrAPo2"],
                    17: ["Compendium.dnd5e-gh.classfeatures-gh.Xkztvs5QGhqgzqYc"]
                }
            }
        }
    },
    druid: {
        subclasses: {
            "circle-of-blood": {
                label: "Circle of Blood",
                source: "GH PG pg. 48",
                features: {
                    2: ["Compendium.dnd5e-gh.classfeatures-gh.ej6WDBitxTFwGacz","Compendium.dnd5e-gh.classfeatures-gh.JP0WP1JwqedNZ3Ky"],
                    6: ["Compendium.dnd5e-gh.classfeatures-gh.BdZD24L52lBxAIm0"],
                    10: ["Compendium.dnd5e-gh.classfeatures-gh.VJ5PqpFkaPbSigUt"],
                    14: ["Compendium.dnd5e-gh.classfeatures-gh.42zWXoH2AXM7bmyH"]
                }
            },
            "circle-of-mutation": {
                label: "Circle of Mutation",
                source: "GH PG pg. 49",
                features: {
                    2: ["Compendium.dnd5e-gh.classfeatures-gh.Nr8NA0Ij9Ply7aj5","Compendium.dnd5e-gh.classfeatures-gh.X9ngQB5WJJzuyWu1"],
                    6: ["Compendium.dnd5e-gh.classfeatures-gh.3ku1FzONYrKqzqnC"],
                    10: ["Compendium.dnd5e-gh.classfeatures-gh.la9AuK0qhwFPmio0"],
                    14: ["Compendium.dnd5e-gh.classfeatures-gh.jFdZAO3VYXDlPyDM"]
                }
            }
        }
    },
    fighter: {
        subclasses: {
            "the-bulwark-warrior": {
                label: "The Bulwark Warrior",
                source: "GH PG pg. 51"
            },
            "living-crucible": {
                label: "Living Crucible",
                source: "GH PG pg. 52"
            }
        }
    },
    monk: {
        subclasses: {
            "way-of-the-leaden-crown": {
                label: "Way of the Leaden Crown",
                source: "GH PG pg. 54"
            },
            "way-of-pride": {
                label: "Way of Pride",
                source: "GH PG pg. 55"
            }
        }
    },
    paladin: {
        subclasses: {
            "oath-of-pestilence": {
                label: "Oath of Pestilence",
                source: "GH PG pg. 56"
            },
            "oath-of-zeal": {
                label: "Oath of Zeal",
                source: "GH PG pg. 58"
            }
        }
    },
    ranger: {
        subclasses: {
            "green-reaper": {
                label: "Green Reaper",
                source: "GH PG pg. 60"
            },
            "vermin-lord": {
                label: "Vermin Lord",
                source: "GH PG pg. 62"
            }
        }
    },
    rogue: {
        subclasses: {
            "highway-rider": {
                label: "Highway Rider",
                source: "GH PG pg. 64"
            },
            "misfortune-bringer": {
                label: "Misfortune Bringer",
                source: "GH PG pg. 66"
            }
        }
    },
    sorcerer: {
        subclasses: {
            "haunted": {
                label: "Haunted",
                source: "GH PG pg. 68"
            },
            "wretched-bloodline": {
                label: "Wretched Bloodline",
                source: "GH PG pg. 70"
            }
        }
    },
    warlock: {
        subclasses: {
            "the-first-vampire": {
                label: "The First Vampire",
                source: "GH PG pg. 72",
                features: {
                    1: ["Compendium.dnd5e-gh.classfeatures-gh.peYCYAPbGzwIkun4"]
                }
            },
            "the-parasite": {
                label: "The Parasite",
                source: "GH PG pg. 73"
            }
        }
    },
    wizard: {
        subclasses: {
            "plague-doctor": {
                label: "Plague Doctor",
                source: "GH PG pg. 76"
            },
            "school-of-sangromancy": {
                label: "School of Sangromancy",
                source: "GH PG pg. 77"
            }
        }
    }
}