import { GH5E } from "./module/config.js";
import Item5eGH from "./module/item/entity.js";

Hooks.on("init", () => {
    CONFIG.GH5E = GH5E;
})

/**
 * Perform one-time pre-localization and sorting of some configuration objects
 */
Hooks.once("setup", function () {
    const localizeKeys = [
        "armorClasses.label", "characterFlags.name", "characterFlags.hint", "characterFlags.section"
    ];
    preLocalizeConfig(CONFIG.GH5E, localizeKeys);
});

/* -------------------------------------------- */

/**
 * Localize and sort configuration values
 * @param {object} config           The configuration object being prepared
 * @param {string[]} localizeKeys   An array of keys to localize
 * @param {string[]} sortKeys       An array of keys to sort
 */
function preLocalizeConfig(config, localizeKeys) {

    // Localize Objects
    for (const key of localizeKeys) {
        if (key.includes(".")) {
            const [inner, label] = key.split(".");
            _localizeObject(config[inner], label);
        }
        else _localizeObject(config[key]);
    }
}

/* -------------------------------------------- */

/**
 * Localize the values of a configuration object by translating them in-place.
 * @param {object} obj                The configuration object to localize
 * @param {string} [key]              An inner key which should be localized
 * @private
 */
function _localizeObject(obj, key) {
    for (const [k, v] of Object.entries(obj)) {

        // String directly
        if (typeof v === "string") {
            obj[k] = game.i18n.localize(v);
            continue;
        }

        // Inner object
        if ((typeof v !== "object") || !(key in v)) {
            console.error(new Error("Configuration values must be a string or inner object for pre-localization"));
            continue;
        }
        v[key] = game.i18n.localize(v[key]);
    }
}

Hooks.on("ready", function () {
    //replace Item5e with Item5eGH to include grim hollow item rules
    game.dnd5e.entities.Item5e.prototype.rollDamage = Item5eGH.prototype.rollDamage;

    /* 
        Merge GH subclasses and class features into main DND5E game loop
    */
    var classFeatures = CONFIG.DND5E.classFeatures;
    var classFeaturesGH = CONFIG.GH5E.classFeatures;

    //add new subclasses to the DND5E configuration
    Object.keys(classFeatures).forEach(function (cls) {
        Object.keys(classFeaturesGH[cls].subclasses).forEach(function (subcls) {
            classFeatures[cls].subclasses[subcls] = classFeaturesGH[cls].subclasses[subcls];
        });
    });

    /* 
        Merge GH armor classes into main DND5E game loop
    */
    var armorClasses = CONFIG.DND5E.armorClasses;
    var armorClassesGH = CONFIG.GH5E.armorClasses;

    Object.keys(armorClassesGH).forEach(function (ac) {
        armorClasses[ac] = armorClassesGH[ac];
    });

    //add GH5E character flags to DND5E character flags
    var characterFlags = CONFIG.DND5E.characterFlags;
    var characterFlagsGH = CONFIG.GH5E.characterFlags;

    Object.keys(characterFlagsGH).forEach(function (cf) {
        characterFlags[cf] = characterFlagsGH[cf];
    })

    CONFIG.DND5E.allowedActorFlags = CONFIG.DND5E.allowedActorFlags.concat(Object.keys(CONFIG.GH5E.characterFlags));
});